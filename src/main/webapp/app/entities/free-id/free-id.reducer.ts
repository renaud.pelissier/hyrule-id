import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IFreeID, defaultValue } from 'app/shared/model/free-id.model';

export const ACTION_TYPES = {
  FETCH_FREEID_LIST: 'freeID/FETCH_FREEID_LIST',
  FETCH_FREEID: 'freeID/FETCH_FREEID',
  CREATE_FREEID: 'freeID/CREATE_FREEID',
  UPDATE_FREEID: 'freeID/UPDATE_FREEID',
  DELETE_FREEID: 'freeID/DELETE_FREEID',
  RESET: 'freeID/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFreeID>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type FreeIDState = Readonly<typeof initialState>;

// Reducer

export default (state: FreeIDState = initialState, action): FreeIDState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FREEID_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FREEID):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_FREEID):
    case REQUEST(ACTION_TYPES.UPDATE_FREEID):
    case REQUEST(ACTION_TYPES.DELETE_FREEID):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_FREEID_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FREEID):
    case FAILURE(ACTION_TYPES.CREATE_FREEID):
    case FAILURE(ACTION_TYPES.UPDATE_FREEID):
    case FAILURE(ACTION_TYPES.DELETE_FREEID):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_FREEID_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_FREEID):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_FREEID):
    case SUCCESS(ACTION_TYPES.UPDATE_FREEID):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_FREEID):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/free-ids';

// Actions

export const getEntities: ICrudGetAllAction<IFreeID> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FREEID_LIST,
    payload: axios.get<IFreeID>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IFreeID> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FREEID,
    payload: axios.get<IFreeID>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IFreeID> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FREEID,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFreeID> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FREEID,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFreeID> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FREEID,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
