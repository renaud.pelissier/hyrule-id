import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FreeID from './free-id';
import FreeIDDetail from './free-id-detail';
import FreeIDUpdate from './free-id-update';
import FreeIDDeleteDialog from './free-id-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FreeIDUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FreeIDUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FreeIDDetail} />
      <ErrorBoundaryRoute path={match.url} component={FreeID} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={FreeIDDeleteDialog} />
  </>
);

export default Routes;
