export interface IFreeID {
  id?: number;
}

export const defaultValue: Readonly<IFreeID> = {};
