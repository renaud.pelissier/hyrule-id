export interface IDocument {
  id?: number;
  privateData?: string;
}

export const defaultValue: Readonly<IDocument> = {};
