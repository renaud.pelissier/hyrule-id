package fr.dalae.hid.web.rest;

import fr.dalae.hid.domain.FreeID;
import fr.dalae.hid.service.FreeIDService;
import fr.dalae.hid.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link FreeID}.
 */
@RestController
@RequestMapping("/api")
public class FreeIDResource {

    private final Logger log = LoggerFactory.getLogger(FreeIDResource.class);

    private static final String ENTITY_NAME = "freeID";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FreeIDService freeIDService;

    public FreeIDResource(FreeIDService freeIDService) {
        this.freeIDService = freeIDService;
    }

    /**
     * {@code POST  /free-ids} : Create a new freeID.
     *
     * @param freeID the freeID to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new freeID, or with status {@code 400 (Bad Request)} if the freeID has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/free-ids")
    public ResponseEntity<FreeID> createFreeID(@RequestBody FreeID freeID) throws URISyntaxException {
        log.debug("REST request to save FreeID : {}", freeID);
        if (freeID.getId() != null) {
            throw new BadRequestAlertException("A new freeID cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FreeID result = freeIDService.save(freeID);
        return ResponseEntity.created(new URI("/api/free-ids/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /free-ids} : Updates an existing freeID.
     *
     * @param freeID the freeID to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated freeID,
     * or with status {@code 400 (Bad Request)} if the freeID is not valid,
     * or with status {@code 500 (Internal Server Error)} if the freeID couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/free-ids")
    public ResponseEntity<FreeID> updateFreeID(@RequestBody FreeID freeID) throws URISyntaxException {
        log.debug("REST request to update FreeID : {}", freeID);
        if (freeID.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FreeID result = freeIDService.save(freeID);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, freeID.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /free-ids} : get all the freeIDS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of freeIDS in body.
     */
    @GetMapping("/free-ids")
    public ResponseEntity<List<FreeID>> getAllFreeIDS(Pageable pageable) {
        log.debug("REST request to get a page of FreeIDS");
        Page<FreeID> page = freeIDService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /free-ids/:id} : get the "id" freeID.
     *
     * @param id the id of the freeID to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the freeID, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/free-ids/{id}")
    public ResponseEntity<FreeID> getFreeID(@PathVariable Long id) {
        log.debug("REST request to get FreeID : {}", id);
        Optional<FreeID> freeID = freeIDService.findOne(id);
        return ResponseUtil.wrapOrNotFound(freeID);
    }

    /**
     * {@code DELETE  /free-ids/:id} : delete the "id" freeID.
     *
     * @param id the id of the freeID to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/free-ids/{id}")
    public ResponseEntity<Void> deleteFreeID(@PathVariable Long id) {
        log.debug("REST request to delete FreeID : {}", id);
        freeIDService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /free-ids/take} : take (get and remove) a randomly chosen freeID.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the freeID, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/free-ids/take")
    public ResponseEntity<FreeID> getFreeID() {
        log.debug("REST request to take a FreeID");
        FreeID freeID = freeIDService.takeRandomly();
        return ResponseUtil.wrapOrNotFound(Optional.of(freeID));
    }

    /**
     * {@code GET  /free-ids/reset} : reset and regenerate all freeIDs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the freeID, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/free-ids/reset")
    public ResponseEntity<FreeID> resetFreeID() {
        log.debug("REST request to reset FreeIDs");
        freeIDService.resetAll();
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "FreeIDs reset.","")).build();
    }
}
