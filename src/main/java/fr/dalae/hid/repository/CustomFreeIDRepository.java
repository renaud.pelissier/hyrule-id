package fr.dalae.hid.repository;

import fr.dalae.hid.domain.FreeID;

public interface CustomFreeIDRepository {
    FreeID takeRandomly();

    void resetAllIds();
}
