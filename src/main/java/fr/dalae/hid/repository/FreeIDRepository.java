package fr.dalae.hid.repository;

import fr.dalae.hid.domain.FreeID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FreeID entity.
 */
@Repository
public interface FreeIDRepository extends JpaRepository<FreeID, Long>, CustomFreeIDRepository {
    long count();
}
