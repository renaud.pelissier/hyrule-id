package fr.dalae.hid.repository;

import fr.dalae.hid.domain.FreeID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Repository
public class CustomFreeIDRepositoryImpl implements CustomFreeIDRepository {

    @Value("${application.max-id}")
    int maxId;


    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void resetAllIds() {
        em.createQuery("DELETE FROM FreeID").executeUpdate();
        for (int i = 0; i <= maxId; i++) {
            FreeID freeID = new FreeID();
            freeID.setId((long) i);
            em.persist(freeID);
            if (i % 100000 == 0) {
                em.flush();
            }
        }
    }

    @Override
    @Transactional
    public FreeID takeRandomly() {
        TypedQuery<FreeID> query = em.createQuery("FROM FreeID f WHERE f.id >= ?1", FreeID.class);
        int randId = new Random().nextInt(maxId);
        Optional<FreeID> id = getFirstWithParameter(query, (long) randId);

        //If no ID available after the given ID, let's try again from O.
        if (!id.isPresent()) {
            id = getFirstWithParameter(query, 0L);
        }

        if (!id.isPresent())
            throw new RuntimeException("No result found. This is very unlikely that all the Free IDs were assigned.");

        FreeID freeID = id.get();

        em.remove(freeID);

        return freeID;
    }

    private Optional<FreeID> getFirstWithParameter(TypedQuery<FreeID> query, Long id) {
        query.setParameter(1, id);
        query.setMaxResults(1);
        List<FreeID> ids = query.getResultList();
        if (ids.isEmpty())
            return Optional.empty();
        else
            return Optional.of(ids.get(0));
    }
}
