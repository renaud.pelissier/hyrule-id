package fr.dalae.hid.service;

import fr.dalae.hid.domain.FreeID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link FreeID}.
 */
public interface FreeIDService {

    /**
     * Save a freeID.
     *
     * @param freeID the entity to save.
     * @return the persisted entity.
     */
    FreeID save(FreeID freeID);

    /**
     * Get all the freeIDS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FreeID> findAll(Pageable pageable);


    /**
     * Get the "id" freeID.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FreeID> findOne(Long id);

    /**
     * Delete the "id" freeID.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    /**
     * Pick a random "id" freeID.
     *
     * @return the entity.
     */
    FreeID takeRandomly();

    /**
     * Regenerate all the freeID entries.
     *
     */
    void resetAll();
}
