package fr.dalae.hid.service.impl;

import fr.dalae.hid.domain.FreeID;
import fr.dalae.hid.repository.FreeIDRepository;
import fr.dalae.hid.service.FreeIDService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link FreeID}.
 */
@Service
@Transactional
public class FreeIDServiceImpl implements FreeIDService {

    private final Logger log = LoggerFactory.getLogger(FreeIDServiceImpl.class);

    private final FreeIDRepository freeIDRepository;

    public FreeIDServiceImpl(FreeIDRepository freeIDRepository) {
        this.freeIDRepository = freeIDRepository;
    }

    @Override
    public FreeID save(FreeID freeID) {
        log.debug("Request to save FreeID : {}", freeID);
        return freeIDRepository.save(freeID);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FreeID> findAll(Pageable pageable) {
        log.debug("Request to get all FreeIDS");
        return freeIDRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<FreeID> findOne(Long id) {
        log.debug("Request to get FreeID : {}", id);
        return freeIDRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FreeID : {}", id);
        freeIDRepository.deleteById(id);
    }

    @Override
    public FreeID takeRandomly() {
        log.debug("Request to take a random FreeID");
        return freeIDRepository.takeRandomly();
    }

    @Override
    public void resetAll() {
        log.debug("Request to reset all IDs");
        freeIDRepository.resetAllIds();
    }
}
