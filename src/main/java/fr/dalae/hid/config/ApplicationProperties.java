package fr.dalae.hid.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Hyrule Id.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private int maxId = 0;

    public void setMaxId(int maxId) {
        this.maxId = maxId;
    }

    public int getMaxId() {
        return maxId;
    }
}
