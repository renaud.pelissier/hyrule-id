package fr.dalae.hid.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A FreeID.
 */
@Entity
@Table(name = "free_id")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FreeID implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FreeID)) {
            return false;
        }
        return id != null && id.equals(((FreeID) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FreeID{" +
            "id=" + getId() +
            "}";
    }
}
