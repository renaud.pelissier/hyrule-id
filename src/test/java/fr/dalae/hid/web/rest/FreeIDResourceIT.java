package fr.dalae.hid.web.rest;

import fr.dalae.hid.domain.FreeID;
import fr.dalae.hid.HyruleIdApp;
import fr.dalae.hid.repository.FreeIDRepository;
import fr.dalae.hid.service.FreeIDService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FreeIDResource} REST controller.
 */
@SpringBootTest(classes = HyruleIdApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FreeIDResourceIT {

    @Autowired
    private FreeIDRepository freeIDRepository;

    @Autowired
    private FreeIDService freeIDService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFreeIDMockMvc;

    private FreeID freeID;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FreeID createEntity(EntityManager em) {
        FreeID freeID = new FreeID();
        return freeID;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FreeID createUpdatedEntity(EntityManager em) {
        FreeID freeID = new FreeID();
        return freeID;
    }

    @BeforeEach
    public void initTest() {
        freeID = createEntity(em);
    }

    @Test
    @Transactional
    public void createFreeID() throws Exception {
        int databaseSizeBeforeCreate = freeIDRepository.findAll().size();
        // Create the FreeID
        restFreeIDMockMvc.perform(post("/api/free-ids")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(freeID)))
            .andExpect(status().isCreated());

        // Validate the FreeID in the database
        List<FreeID> freeIDList = freeIDRepository.findAll();
        assertThat(freeIDList).hasSize(databaseSizeBeforeCreate + 1);
        FreeID testFreeID = freeIDList.get(freeIDList.size() - 1);
    }

    @Test
    @Transactional
    public void createFreeIDWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = freeIDRepository.findAll().size();

        // Create the FreeID with an existing ID
        freeID.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFreeIDMockMvc.perform(post("/api/free-ids")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(freeID)))
            .andExpect(status().isBadRequest());

        // Validate the FreeID in the database
        List<FreeID> freeIDList = freeIDRepository.findAll();
        assertThat(freeIDList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFreeIDS() throws Exception {
        // Initialize the database
        freeIDRepository.saveAndFlush(freeID);

        // Get all the freeIDList
        restFreeIDMockMvc.perform(get("/api/free-ids?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(freeID.getId().intValue())));
    }

    @Test
    @Transactional
    public void getFreeID() throws Exception {
        // Initialize the database
        freeIDRepository.saveAndFlush(freeID);

        // Get the freeID
        restFreeIDMockMvc.perform(get("/api/free-ids/{id}", freeID.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(freeID.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFreeID() throws Exception {
        // Get the freeID
        restFreeIDMockMvc.perform(get("/api/free-ids/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFreeID() throws Exception {
        // Initialize the database
        freeIDService.save(freeID);

        int databaseSizeBeforeUpdate = freeIDRepository.findAll().size();

        // Update the freeID
        FreeID updatedFreeID = freeIDRepository.findById(freeID.getId()).get();
        // Disconnect from session so that the updates on updatedFreeID are not directly saved in db
        em.detach(updatedFreeID);

        restFreeIDMockMvc.perform(put("/api/free-ids")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFreeID)))
            .andExpect(status().isOk());

        // Validate the FreeID in the database
        List<FreeID> freeIDList = freeIDRepository.findAll();
        assertThat(freeIDList).hasSize(databaseSizeBeforeUpdate);
        FreeID testFreeID = freeIDList.get(freeIDList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingFreeID() throws Exception {
        int databaseSizeBeforeUpdate = freeIDRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFreeIDMockMvc.perform(put("/api/free-ids")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(freeID)))
            .andExpect(status().isBadRequest());

        // Validate the FreeID in the database
        List<FreeID> freeIDList = freeIDRepository.findAll();
        assertThat(freeIDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFreeID() throws Exception {
        // Initialize the database
        freeIDService.save(freeID);

        int databaseSizeBeforeDelete = freeIDRepository.findAll().size();

        // Delete the freeID
        restFreeIDMockMvc.perform(delete("/api/free-ids/{id}", freeID.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FreeID> freeIDList = freeIDRepository.findAll();
        assertThat(freeIDList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
