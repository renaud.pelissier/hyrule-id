package fr.dalae.hid.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.dalae.hid.web.rest.TestUtil;

public class FreeIDTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FreeID.class);
        FreeID freeID1 = new FreeID();
        freeID1.setId(1L);
        FreeID freeID2 = new FreeID();
        freeID2.setId(freeID1.getId());
        assertThat(freeID1).isEqualTo(freeID2);
        freeID2.setId(2L);
        assertThat(freeID1).isNotEqualTo(freeID2);
        freeID1.setId(null);
        assertThat(freeID1).isNotEqualTo(freeID2);
    }
}
