package fr.dalae.hid.repository;

import fr.dalae.hid.HyruleIdApp;
import fr.dalae.hid.domain.FreeID;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = HyruleIdApp.class)
public class FreeIDRepositoryIT {

    private final Logger log = LoggerFactory.getLogger(FreeIDRepositoryIT.class);

    @Autowired
    private FreeIDRepository freeIDRepository;

    @Value("${application.max-id}")
    private int maxId;

    @Test
    public void testTakeClosestIdAfter() {
        FreeID optionalId = freeIDRepository.takeRandomly();
        log.debug("Found item {}", optionalId);
    }

    @Test
    public void testResetAllIds() {
        log.info("resetAllIds({)");
        freeIDRepository.resetAllIds();

        log.info("resetAllIds done.");

        Assert.assertEquals(freeIDRepository.count(),maxId+1);
    }
}
