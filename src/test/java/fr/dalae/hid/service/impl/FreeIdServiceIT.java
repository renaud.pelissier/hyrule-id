package fr.dalae.hid.service.impl;

import fr.dalae.hid.domain.FreeID;
import fr.dalae.hid.HyruleIdApp;
import fr.dalae.hid.repository.FreeIDRepository;
import fr.dalae.hid.service.FreeIDService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = HyruleIdApp.class)
public class FreeIdServiceIT {

    private final Logger log = LoggerFactory.getLogger(FreeIdServiceIT.class);

    @Autowired
    private FreeIDRepository freeIDRepository;

    @Value("${application.max-id}")
    private int maxId;

    private FreeIDService freeIDService;

    @BeforeEach
    public void init() {
        freeIDService = new FreeIDServiceImpl(freeIDRepository);
    }

    @Test
    public void testTakeRandomly() {
        FreeID optionalId = freeIDService.takeRandomly();
        log.debug("Found item {}", optionalId);
    }

    @Test
    public void testPopulate() {
        log.debug("resetAll()");
        freeIDService.resetAll();
        log.debug("resetAll done.");
    }
}
